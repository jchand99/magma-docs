---
id: createcursor
title: CreateCursor
sidebar_label: CreateCursor
---

## CreateCursor
```c#
public static IntPtr CreateCursor( Image image, int xhot, int yhot )
```

Creates a new custom cursor image that can be set for a window with glfwSetCursor. The cursor can be destroyed with glfwDestroyCursor. Any remaining cursors are destroyed by glfwTerminate.

The pixels are 32-bit, little-endian, non-premultiplied RGBA, i.e. eight bits per channel with the red channel first. They are arranged canonically as packed sequential rows, starting from the top-left corner.

The cursor hotspot is specified in pixels, relative to the upper-left corner of the cursor image. Like all other coordinate systems in GLFW, the X-axis points to the right and the Y-axis points down.

### Parameters
    [in]	image	The desired cursor image.
    [in]	xhot	The desired x-coordinate, in pixels, of the cursor hotspot.
    [in]	yhot	The desired y-coordinate, in pixels, of the cursor hotspot.

### Returns
    The handle of the created cursor, or IntPtr.Zero if an error occurred.

### Errors
    Possible errors include GLFW_NOT_INITIALIZED and GLFW_PLATFORM_ERROR.

### Pointer lifetime
    The specified image data is copied before this function returns.

### Thread safety
    This function must only be called from the main thread.

### See also
[DestroyCursor](destroycursor.md)
[CreateStandardCursor](createstandardcursor.md)

Since
    Added in GLFW version 3.1.
