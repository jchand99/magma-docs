---
id: createstandardcursor
title: CreateStandardCursor
sidebar_label: CreateStandardCursor
---

## CreateStandardCursor
```c#
public static IntPtr CreateStandardCursor( int shape )
```

Returns a cursor with a standard shape, that can be set for a window with glfwSetCursor.

### Parameters
    [in]	shape	One of the standard shapes.

### Returns
    A new cursor ready to use or IntPtr.Zero if an error occurred.

### Errors
    Possible errors include GLFW_NOT_INITIALIZED, GLFW_INVALID_ENUM and GLFW_PLATFORM_ERROR.

### Thread safety
    This function must only be called from the main thread.

### See also
[CreateCursor](createcursor.md)

Since
    Added in GLFW version 3.1.

