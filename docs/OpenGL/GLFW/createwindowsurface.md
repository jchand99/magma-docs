---
id: createwindowsurface
title: CreateWindowSurface
sidebar_label: CreateWindowSurface
---

## CreateWindowSurface
```c#
public static int CreateWindowSurface( IntPtr instance, IntPtr window, IntPtr allocator, out IntPtr surface);
```

This function creates a Vulkan surface for the specified window.

If the Vulkan loader or at least one minimally functional ICD were not found, this function returns VK_ERROR_INITIALIZATION_FAILED and generates a GLFW_API_UNAVAILABLE error. Call [VulkanSupported](vulkansupported.md) to check whether Vulkan is at least minimally available.

If the required window surface creation instance extensions are not available or if the specified instance was not created with these extensions enabled, this function returns VK_ERROR_EXTENSION_NOT_PRESENT and generates a GLFW_API_UNAVAILABLE error. Call [GetRequiredInstanceExtensions](getrequiredinstanceextensions.md) to check what instance extensions are required.

The window surface cannot be shared with another API so the window must have been created with the client api hint set to GLFW_NO_API otherwise it generates a GLFW_INVALID_VALUE error and returns VK_ERROR_NATIVE_WINDOW_IN_USE_KHR.

The window surface must be destroyed before the specified Vulkan instance. It is the responsibility of the caller to destroy the window surface. GLFW does not destroy it for you. Call vkDestroySurfaceKHR to destroy the surface.

### Parameters
    [in]	instance	The Vulkan instance to create the surface in.
    [in]	window	The window to create the surface for.
    [in]	allocator	The allocator to use, or NULL to use the default allocator.
    [out]	surface	Where to store the handle of the surface. This is set to VK_NULL_HANDLE if an error occurred.

### Returns
    VK_SUCCESS if successful, or a Vulkan error code if an error occurred.

### Errors
    Possible errors include GLFW_NOT_INITIALIZED, GLFW_API_UNAVAILABLE, GLFW_PLATFORM_ERROR and GLFW_INVALID_VALUE

### Remarks
    If an error occurs before the creation call is made, GLFW returns the Vulkan error code most appropriate for the error. Appropriate use of VulkanSupported and GetRequiredInstanceExtensions should eliminate almost all occurrences of these errors.
    macOS: This function currently only supports the VK_MVK_macos_surface extension from MoltenVK.
    macOS: This function creates and sets a CAMetalLayer instance for the window content view, which is required for MoltenVK to function.

### Thread safety
    This function may be called from any thread. For synchronization details of Vulkan objects, see the Vulkan specification.

### See also
[GetRequiredInstanceExtensions](getrequiredinstanceextensions.md)

Since
    Added in GLFW version 3.2.

