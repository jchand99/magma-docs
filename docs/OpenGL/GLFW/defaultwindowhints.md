---
id: defaultwindowhints
title: DefaultWindowHints
sidebar_label: DefaultWindowHints
---

## DefaultWindowHints
```c#
public static void DefaultWindowHints( void );
```

This function resets all window hints to their default values.

### Errors
    Possible errors include GLFW_NOT_INITIALIZED.

### Thread safety
    This function must only be called from the main thread.

### See also
[WindowHint](windowhint.md)
[WindowHintString](windowhintstring.md)

Since
    Added in GLFW version 3.0.

