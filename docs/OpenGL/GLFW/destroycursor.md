---
id: destroycursor
title: DestroyCursor
sidebar_label: DestroyCursor
---

## DestroyCursor
```c#
public static void DestroyCursor( IntPtr cursor );
```

This function destroys a cursor previously created with [CreateCursor](createcursor.md). Any remaining cursors will be destroyed by [Terminate](terminate.md).

If the specified cursor is current for any window, that window will be reverted to the default cursor. This does not affect the cursor mode.

### Parameters
    [in]	cursor	The cursor object to destroy.

### Errors
    Possible errors include GLFW_NOT_INITIALIZED and GLFW_PLATFORM_ERROR.

### Reentrancy
    This function must not be called from a callback.

### Thread safety
    This function must only be called from the main thread.

### See also
[CreateCursor](createcursor.md)

Since
    Added in GLFW version 3.1.

