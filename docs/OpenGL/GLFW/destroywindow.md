---
id: destroywindow
title: DestroyWindow
sidebar_label: DestroyWindow
---

## DestroyWindow
```c#
public static void DestroyWindow( IntPtr window );
```

This function destroys the specified window and its context. On calling this function, no further callbacks will be called for that window.

If the context of the specified window is current on the main thread, it is detached before being destroyed.

### Parameters
    [in]	window	The window to destroy.

### Errors
    Possible errors include GLFW_NOT_INITIALIZED and GLFW_PLATFORM_ERROR.

### Note
    The context of the specified window must not be current on any other thread when this function is called.

### Reentrancy
    This function must not be called from a callback.

### Thread safety
    This function must only be called from the main thread.

### See also
[CreateWindow](createwindow.md)

Since
    Added in GLFW version 3.0. Replaces glfwCloseWindow.

