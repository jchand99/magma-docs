---
id: init
title: Init
sidebar_label: Init
---

## Init

```c#
private static bool Init( void );
```

This function initializes the GLFW library. Before most GLFW functions can be used, GLFW must be initialized, and before an application terminates GLFW should be terminated in order to free any resources allocated during or after initialization.

If this function fails, it calls glfwTerminate before returning. If it succeeds, you should call glfwTerminate before the application exits.

Additional calls to this function after successful initialization but before termination will return true immediately.

### Returns
    true if successful, or false if an error occurred.

### Errors
    Possible errors include GLFW_PLATFORM_ERROR.

### Remarks
    macOS: This function will change the current directory of the application to the Contents/Resources subdirectory of the application's bundle, if present. This can be disabled with the GLFW_COCOA_CHDIR_RESOURCES init hint.

### Thread safety
    This function must only be called from the main thread.

### See also
[Terminate](terminate.md)

Since
    Added in GLFW version 1.0.
