---
id: terminate
title: Terminate
sidebar_label: Terminate
---

## Terminate

```c#
public static void Terminate( void );
```

This function destroys all remaining windows and cursors, restores any modified gamma ramps and frees any other allocated resources. Once this function is called, you must again call glfwInit successfully before you will be able to use most GLFW functions.

If GLFW has been successfully initialized, this function should be called before the application exits. If initialization fails, there is no need to call this function, as it is called by glfwInit before it returns failure.

### Errors
    Possible errors include GLFW_PLATFORM_ERROR.

### Remarks
    This function may be called before glfwInit.

### *Warning*
    The contexts of any remaining windows must not be current on any other thread when this function is called.

### Reentrancy
    This function must not be called from a callback.

### Thread safety
    This function must only be called from the main thread.

### See also
[Init](init.md)

Since
    Added in GLFW version 1.0.
