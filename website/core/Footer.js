/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

class Footer extends React.Component {
  docUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    const docsUrl = this.props.config.docsUrl;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
    const langPart = `${language ? `${language}/` : ''}`;
    return `${baseUrl}${docsPart}${langPart}${doc}`;
  }

  pageUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    return baseUrl + (language ? `${language}/` : '') + doc;
  }

  render() {
    return (
      <footer className="nav-footer" id="footer">
        <section className="sitemap">
          <a href={this.props.config.baseUrl} className="nav-home">
            {this.props.config.footerIcon && (
              <img
                src={this.props.config.baseUrl + this.props.config.footerIcon}
                alt={this.props.config.title}
                width="66"
                height="58"
              />
            )}
          </a>
          <div>
            <h5>Docs</h5>
            <a href={this.docUrl('gl.html', this.props.language)}>
              OpenGL
            </a>
            <a href={this.docUrl('al.html', this.props.language)}>
              OpenAL
            </a>
            <a href={this.docUrl('vulkan.html', this.props.language)}>
              Vulkan
            </a>
            <a href={this.docUrl('vector.html', this.props.language)}>
              Maths
            </a>
            <a href={this.docUrl('object-loader.html', this.props.language)}>
              Object Loader
            </a>
          </div>
          <div>
            <h5>Community</h5>
            <a href="https://discordapp.com/">Project Chat</a>
            <a href="https://gitlab.com/jchand99/magma">Magma Repo</a>
          </div>
          <div>
            <h5>More</h5>
            <a href={`${this.props.config.baseUrl}blog`}>Blog</a>
            <a href="https://gitlab.com/pages/docusaurus" target="_blank">Built by GitLab Pages</a>
            <a href="https://github.com/facebook/docusaurus" target="_blank">Docusaurus on GitHub</a>
            <a
              className="github-button"
              href={this.props.config.repoUrl}
              data-icon="octicon-star"
              data-count-href="/facebook/docusaurus/stargazers"
              data-show-count="true"
              data-count-aria-label="# stargazers on GitHub"
              aria-label="Star this project on GitHub">
              Star
            </a>
            {this.props.config.twitterUsername && (
              <div className="social">
                <a
                  href={`https://twitter.com/${this.props.config.twitterUsername}`}
                  className="twitter-follow-button">
                  Follow @{this.props.config.twitterUsername}
                </a>
              </div>
            )}
            {this.props.config.facebookAppId && (
              <div className="social">
                <div
                  className="fb-like"
                  data-href={this.props.config.url}
                  data-colorscheme="dark"
                  data-layout="standard"
                  data-share="true"
                  data-width="225"
                  data-show-faces="false"
                />
              </div>
            )}
          </div>
          <div>
            <h5>Docs-site Icon Authors</h5>
            <div className="footerText">
              <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a>
              <a href="https://www.flaticon.com/authors/photo3idea-studio" title="photo3idea_studio">photo3idea_studio</a>
              <a href="https://www.flaticon.com/authors/icongeek26" title="Icongeek26">Icongeek26</a>
              <a href="https://www.flaticon.com/authors/pixel-perfect" title="Pixel perfect">Pixel perfect</a>
              <a href="https://www.flaticon.com/authors/iconixar" title="iconixar">iconixar</a>
              from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
            </div>

          </div>
        </section>

        <a
          href="https://opensource.facebook.com/"
          target="_blank"
          rel="noreferrer noopener"
          className="fbOpenSource">
          <img
            src={`${this.props.config.baseUrl}img/oss_logo.png`}
            alt="Facebook Open Source"
            width="170"
            height="45"
          />
        </a>
        <section className="copyright">{this.props.config.copyright}</section>
      </footer>
    );
  }
}

module.exports = Footer;
